const Vue = require('nativescript-vue/dist/index')
const http = require("http")

import JsComponent from './JsComponent';
import VueComponent from './VueComponent';

import './app.scss'

Vue.registerElement('CardView', () => require("nativescript-cardview").CardView);


Vue.prototype.$http = http

new Vue({
    components: {
        JsComponent,
        VueComponent
    },

    template: `
    <page ref="page">
      <stack-layout>
      <CardView>
        <js-component></js-component>
        </CardView>
      <CardView>
        <vue-component></vue-component>
      </CardView>
      </stack-layout>
    </page>
  `,
    methods: {}
}).$start()